FROM node:10-alpine

WORKDIR /auc_front

ENV PATH /auc_front/node_modules/.bin:$PATH

COPY package.json /auc_front/package.json
COPY src /auc_front/src
COPY public /auc_front/public
COPY .env.development /auc_front/.env

RUN npm install --silent

CMD ["npm", "start"]