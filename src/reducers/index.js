import {createStore, combineReducers, applyMiddleware, compose} from 'redux'
import {actionConsts, DEFAULT_FILTER} from "../config";
import thunk from 'redux-thunk';


const filter = (state = DEFAULT_FILTER, action) => {
    if (action.type === actionConsts.TOGGLE_FILTER) {
        return action.form;
    } else {
        return state;
    }
};

const offers = (state = {data: []}, action) => {
    switch (action.type) {
        case actionConsts.GET_OFFERS_SUCCESS:
            console.log(action.data);
            if (action.data.type === 'all') {
                return {data: action.data.data}
            }
            if (action.data.type === 'line') {
                let added = false;
                const data = state.data.map(item => {
                    if (item.id === action.data.data.id) {
                        added = true;
                        return action.data.data;
                    }

                    return item;
                });
                //add new item
                if (!added) {
                    data.push(action.data.data);
                }
                return {data: data}
            }

            return {date: []};
        case actionConsts.FAVORITE_CHANGED:
            const data = state.data.map(item => {
                if (item.id === action.product_variant_id) {
                    return {...item, additional: action.data};
                }

                return item;
            });

            return {data: data};
        default:
            return state;
    }
};

const brands = (state = {data: []}, action) => {
    if (action.type === actionConsts.GET_BRANDS_SUCCESS) {
        return {data: action.data};
    } else {
        return state;
    }
};

const categories = (state = {data: []}, action) => {
    if (action.type === actionConsts.GET_CATEGORIES_SUCCESS) {
        return {data: action.data};
    } else {
        return state;
    }
};

const status = (state = {}, action) => {
    let s;
    switch (action.type) {
        case actionConsts.STATUS_CHANGED:
            s = {...state};
            s[action.offer] = {...s[action.offer], manual: true, value: action.value};
            return s;
        case actionConsts.STATUS_PRICE_CHANGED:
            s = {...state};
            s[action.offer] = {...s[action.offer], price: action.value};
            return s;
        case actionConsts.STATUS_REMOVED:
            s = {...state};
            delete s[action.offer];
            return s;
        case actionConsts.STATUS_INIT:
            s = {...state};
            s[action.offer] = {...s[action.offer], manual: false, value: action.value};
            return s;
        default:
            return state;
    }
};


const auth = (state = {loggedIn: false}, action) => {
    if (action.type === actionConsts.USER_DETAILS_RECEIVER) {
        return {loggedIn: true, user: action.user, type: action.user.role};
    } else if (action.type === actionConsts.TYPE_OF_USER_CHANGED) {
        return {...state, type: action.value}
    } else {
        return state;
    }
};

const rootReducer = combineReducers({
    filter,
    offers,
    categories,
    brands,
    status,
    auth
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(rootReducer,
    composeEnhancers(applyMiddleware(thunk)));