import {backApi} from '../config';
import {url} from "../utils";


backApi.defaults.headers.common['user_id'] = 1;
backApi.defaults.headers.common['user_role'] = 'supplier';

export const setFavorite = (variantId, favorite) => {
    return backApi.put(`/offers/${variantId}/favorite`, {"favorite": favorite})
};

export const getUserDetails = () => {
    return backApi.get('/user_details')
};

export const getDocumentDates = () => {
    return backApi.get('/document_dates')
};

export const getDocuments = () => {
    return backApi.get('/documents')
};

export const getUserOrders = (date) => {
    return backApi.get('/user_orders' + (date ? '?date=' + date : ''))
};

export const getBrands = () => {
    return backApi.get('/brands')
};

export const getCategories = () => {
    return backApi.get('/categories')
};

export const getVariants = (category_id, brand_id) => {
    return backApi.get(`/variants?category_id=${category_id}&brand_id=${brand_id}`);
};

export const getDescriptions = (product_id) => {
    return backApi.get(`/descriptions?product_id=${product_id}`);
};

export const addOffer = (variantId, value) => {
    return backApi.put(`/offers/${variantId}/add`, {"value": Number(value)})
};

export const makeBet = (variantId, qty, price) => {
    return backApi.put(`/offers/${variantId}/make_bet`, {"price": Number(price), "qty":Number(qty)})
};

export const lowerBet = (variantId, qty, price) => {
    return backApi.put(`/offers/${variantId}/lower_bet`, {'price': Number(price), 'qty':Number(qty)})
};

export const createRequest = (variantId, qty) => {
    return backApi.put(`/offers/${variantId}/request`, {'qty': Number(qty)});
};

export const refreshOffers = () => {
    return backApi.get(`/offers/refresh`);
};

export const getOrders = (date) => {
    return backApi.get('/orders' + (date ? '?date=' + date : ''));
};

export const productInquiry = (data) => {
    return backApi.post('/offers/inquiry', data);
};


export const setupUser = (userId, role) => {
    backApi.defaults.headers.common['user_id'] = userId;
    backApi.defaults.headers.common['user_role'] = role;
};

let ws;

const heartbeat = () => {
    if (!ws) return;
    if (ws.readyState !== 1) return;
    ws.send("heartbeat");
    setTimeout(heartbeat, 10000);
};

export const getWs = (onMessage) => {
    if (!ws) {
        ws = new WebSocket(url("back/ws"));
        ws.onmessage = onMessage;

        ws.onopen = () => {
            console.log("opened WS");
            ws.send('get_data');
            heartbeat()
        };

        ws.onclose = () => {
            console.log('closed WS');
        };
    }

    return ws;
};