const proxy = require('http-proxy-middleware');

module.exports = function(app) {
    app.use(proxy('/back/**', {
        target: 'http://auc_back:8080/',
        secure: false,
        changeOrigin: true,

    }));
};
