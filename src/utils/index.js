export const url = (s) => {
    let l = window.location;
    return ((l.protocol === "https:") ? "wss://" : "ws://") + l.hostname
        +  (l.port === "3000" ? ":" + 8080 : (((l.port !== "80") && (l.port !== "443")) ? ":" + l.port : ""))
        + l.pathname
        + s;
};

const extractIdsFromFilter = (filter) => {
    return Object.entries(filter)
        .filter(entry => entry[1])
        .map(entry => parseInt(entry[0]));
};


export function filterOffers(data, filter) {

    if (filter) {
        if (filter.brand) {
            let brands = extractIdsFromFilter(filter.brand);
            if (brands.length > 0) {
                data = data.filter(item => brands.includes(item.product_variant.brand.id))
            }
        }
        if (filter.category) {
            let categories = extractIdsFromFilter(filter.category);
            if (categories.length > 0) {
                data = data.filter(item => categories.includes(item.product_variant.category.id))
            }

        }
        if (filter.favorite) {
            data = data.filter(item => item.additional.favorite);
        }
        if (filter.search) {
            data = data.filter(item => item.product_variant.name.toLowerCase().includes(filter.search.toLowerCase()))
        }
        if (filter.inStock || filter.inProcess) {
            data = data.filter(item => item.my_qty > 0)
        }
    }

    return data;
}

