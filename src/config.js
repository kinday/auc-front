import indigo from "@material-ui/core/colors/indigo";
import teal from "@material-ui/core/colors/teal";
import axios from "axios";

export const THEME = {
    palette: {
        primary: indigo,
        secondary: teal,
        type: "dark"
    }
};

export const backApi = axios.create({
    baseURL: '/back'
});

export const actionConsts = {
    GET_OFFERS_SUCCESS: 'GET_OFFERS_SUCCESS',
    GET_BRANDS_SUCCESS: 'GET_BRANDS_SUCCESS',
    GET_CATEGORIES_SUCCESS: 'GET_CATEGORIES_SUCCESS',
    FAVORITE_CHANGED: 'FAVORITE_CHANGED',
    USER_DETAILS_RECEIVER: 'USER_DETAILS_RECEIVER',
    STATUS_CHANGED: 'STATUS_CHANGED',
    STATUS_REMOVED: 'STATUS_REMOVED',
    STATUS_INIT: 'STATUS_INIT',
    STATUS_PRICE_CHANGED: 'STATUS_PRICE_CHANGED',
    TOGGLE_FILTER: 'TOGGLE_FILTER',
    TYPE_OF_USER_CHANGED: 'TYPE_OF_USER_CHANGED',
    STATUS_SAVED: 'STATUS_SAVED',
    GET_ORDERS_SUCCESS: 'GET_ORDERS_SUCCESS',
    THEME_CHANGED: 'THEME_CHANGED'
};

export const DEFAULT_FILTER = {
    brand: {
        "1": false,
        "2": false,
        "3": false
    },
    category: {
        "1": false,
        "2": false
    },
    favorite: false,
    search: ''
};

export const LOWER_DELTA = 1;
export const MAX_REQUEST = 100;
export const MIN_DOCUMENT_DATE = 100;
export const DATE_PATTERN = 'yyyy-MM-dd';

const FORMATTER_MAP = {};

export const currencyFormatter = (currency = 'USD') => {
    let formatter = FORMATTER_MAP[currency];

    if (!formatter) {
        formatter = new Intl.NumberFormat('ru-Ru', {
            style: 'currency',
            currency: currency,
            minimumFractionDigits: 0
        });
        FORMATTER_MAP[currency] = formatter;
    }
    return formatter;
};

export const calcMinRequest = (maxRequest) => {
    return Math.floor(maxRequest / 2);
};



