import React, {useState} from 'react';
import {Provider} from 'react-redux';
import ThemeProvider from "@material-ui/styles/ThemeProvider";
import CssBaseline from '@material-ui/core/CssBaseline';
import {THEME} from './config';
import {store} from './reducers';
import Main from "./components/Main";
import { SnackbarProvider } from 'notistack';
import {createMuiTheme} from "@material-ui/core";


function App() {

    const [theme, setTheme] = useState(THEME);

    const toggleTheme = (type) => {
        if (type !== theme.palette.type) {
            let t = {...theme};
            t['palette'] = {...theme['palette'], 'type': type};
            setTheme(t);
        }
    };

    const muiTheme = createMuiTheme(theme);

    return (
        <Provider store={store}>
            <ThemeProvider theme={muiTheme}>
                <CssBaseline/>
                <SnackbarProvider maxSnack={3} anchorOrigin={{vertical: 'top', horizontal: 'center'}}>
                    <Main toggleTheme={toggleTheme}/>
                </SnackbarProvider>
            </ThemeProvider>
        </Provider>
    );
}

export default App;
