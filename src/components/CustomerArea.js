import React from 'react';
import Customer from "./Customer";
import {useSelector} from "react-redux";
import AuctionStub from "./ActionStub";

const CustomerArea = () => {
    const auth = useSelector(state => state.auth);

    const isAuctionActive = () => {
        const now = new Date();

        return now > new Date(auth.user.startDate) && now < new Date(auth.user.finishDate);
    };

    return isAuctionActive() ? (<Customer/>) : (<AuctionStub/>);
};

export default CustomerArea;

