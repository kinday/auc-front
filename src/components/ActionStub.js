import React from 'react';
import {useSelector} from "react-redux";
import {Paper} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import {useTranslation} from "react-i18next";

const AuctionStub = () => {

    const auth = useSelector(state => state.auth);
    const { t } = useTranslation();


    const isAuctionNotStarted = () => {
        const now = new Date();

        return now < new Date(auth.user.startDate);
    };

    const isAuctionFinished = () => {
        const now = new Date();

        return now > new Date(auth.user.finishDate);
    };


    return (<Paper>
        <Typography align='center'>
            { isAuctionNotStarted() ? t('stub.notStarted') : t('stub.finished')}
        </Typography>
    </Paper>);
};

export default AuctionStub;

