import React from 'react';
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import RadioGroup from "@material-ui/core/RadioGroup";
import Radio from "@material-ui/core/Radio";
import {useDispatch, useSelector} from 'react-redux';
import {refreshOffers, setupUser} from "../services";
import {actionConsts} from "../config";


const RoleSwitcher = () => {

    const auth = useSelector(state => state.auth);

    const [value, setValue] = React.useState(auth.type || 'supplier');
    const dispatch = useDispatch();

    const handleChange = (event) => {

        const role = event.target.value;
        setValue(role);

        setupUser(1, event.target.value);
        dispatch({type: actionConsts.TYPE_OF_USER_CHANGED, value: role});
        refreshOffers();
    };

    return (
        <FormControl component="fieldset">
            <FormLabel component="legend">Type of user</FormLabel>
            <RadioGroup
                aria-label="Type of user"
                name="userType"
                value={value}
                onChange={handleChange}>
                <FormControlLabel value="supplier" control={<Radio />} label="Supplier" />
                <FormControlLabel value="customer" control={<Radio />} label="Customer" />
                <FormControlLabel value="admin" control={<Radio />} label="Admin" />
            </RadioGroup>
        </FormControl>)
};

export default RoleSwitcher;
