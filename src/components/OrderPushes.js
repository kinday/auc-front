import React, {Component} from 'react';
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import Table from "@material-ui/core/Table";
import Box from "@material-ui/core/Box";

import {currencyFormatter} from "../config";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import withStyles from "@material-ui/styles/withStyles";

const styles = theme => ({
    total: {
        margin: theme.spacing(2)
    }
});

class OrderPushes extends Component {
    render() {

        const {order, classes} = this.props;

        return (
            <Box>
                <Grid
                    container
                    direction="column"
                    justify="flex-start"
                    alignItems="stretch"
                >
                    <Grid item>
                        <Typography align='center' variant="h5">
                            Sales order
                        </Typography>
                    </Grid>
                    <Grid item>
                        <Grid
                            container
                            direction="row"
                            justify="flex-end"
                            alignItems="flex-start"
                        >
                            <Grid item>
                                <Typography>
                                    Date: {order.date}
                                    <br/>
                                    Customer: {order.user.name}
                                </Typography>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>#</TableCell>
                                <TableCell>Supplier</TableCell>
                                <TableCell>Name</TableCell>
                                <TableCell>Desc</TableCell>
                                <TableCell>Price</TableCell>
                                <TableCell>Qty</TableCell>
                                <TableCell>Total</TableCell>
                                <TableCell/>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {order.items.map((item, index) =>
                                item.offers.map((offer) => (
                                    <TableRow key={item.id + '-' + offer.supplier.id}>
                                        <TableCell>{index + 1}</TableCell>
                                        <TableCell>{offer.supplier.name}</TableCell>
                                        <TableCell>{item.product_variant.name}</TableCell>
                                        <TableCell>{item.description.name}</TableCell>
                                        <TableCell>{currencyFormatter().format(item.price)}</TableCell>
                                        <TableCell>{offer.qty}</TableCell>
                                        <TableCell>{currencyFormatter().format(item.price * offer.qty)}</TableCell>
                                    </TableRow>
                                )))}
                        </TableBody>
                    </Table>
                </Grid>
                <Grid item>
                    <div className={classes.total}>
                        <Grid

                            spacing={3}
                            container
                            direction="row"
                            justify="flex-end"
                            alignItems="flex-start"
                        >
                            <Grid item>
                                <Typography>
                                    Total: {order.qty}
                                </Typography>
                            </Grid>
                            <Grid item>
                                <Typography>
                                    Total amount: {currencyFormatter().format(order.price)}
                                </Typography>
                            </Grid>
                        </Grid>
                    </div>
                </Grid>
            </Box>
        )
    }
}

export default withStyles(styles)(OrderPushes);