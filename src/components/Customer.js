import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {createSelector} from "reselect";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import FavoriteCheckbox from "./FavoriteCheckbox";
import {makeStyles} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import AddCircleOutlineIcon from "@material-ui/icons/AddCircleOutline";
import RemoveCircleOutlineIcon from "@material-ui/icons/RemoveCircleOutline";

import {calcMinRequest, currencyFormatter, MAX_REQUEST} from "../config";
import {getWs} from "../services";
import {filterOffers} from "../utils";
import {initStatus, makeRequest, receiveOffers, statusChanged} from "../actions";
import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";
import CustomerInquiryDialog from "./CustomerInquiryDialog";


const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing(3),
        overflowX: 'auto',
    },
    table: {
        minWidth: 850,
    },
    request: {
        width: 40,
        textAlign: 'center'
    },
    unsaved: {
        color: theme.palette.secondary.main
    },
    saved: {
        color: theme.palette.text.primary
    },
    buttonPanel: {
        margin: theme.spacing(2)
    }

}));

// NOTE: Переменные в контексте модуля с компонентом — сомнительный подход,
//       т.к. два компонента в разных местах дерева могут внести конфликтующие
//       изменения. По сути, это global mutable state, но в рамках одного
//       модуля. Последствия, тем не менее, те же самые.
const timeouts = {};

const CustomerRequest = (props) => {
    const offer = props.offer;
    const status = useSelector(state => state.status);
    const dispatch = useDispatch();

    useEffect(() => {
        if (!status[offer.id] || !status[offer.id].manual) {
            dispatch(initStatus(offer.id, offer.my_qty));
        }
    }, [offer]);


    const saveQtyChanges = (offer, qty) => {
        if (timeouts[offer.id]) {
            clearTimeout(timeouts[offer.id]);
        }

        timeouts[offer.id] = setTimeout(() => {

            dispatch(makeRequest(offer.id, validate(qty, offer.my_max_qty)));
        }, 1200);
    };

    const validate = (qty, max_qty) => {
        return Math.max(Math.min(Number(qty), MAX_REQUEST), 0, calcMinRequest(max_qty));
    };

    const handleQtyChanged = (offer) => (event) => {
        const qty = event.target.value;
        dispatch(statusChanged(offer.id, qty));
        saveQtyChanges(offer, qty);
    };

    const handleQtyInc = (offer) => (event) => {
        const qty = Number(status[offer.id].value) + 1;
        dispatch(statusChanged(offer.id, qty));
        saveQtyChanges(offer, qty);
    };

    const handleQtyDec = (offer) => (event) => {
        const qty = Number(status[offer.id].value) - 1;
        dispatch(statusChanged(offer.id, qty));
        saveQtyChanges(offer, qty);
    };

    return (
        <TextField
            type="number"
            value={status[offer.id] ? status[offer.id].value : ''}
            onChange={handleQtyChanged(offer)}
            error={status[offer.id] && status[offer.id].manual}
            inputProps={{min: calcMinRequest(offer.my_max_qty), max: MAX_REQUEST, className: props.className}}
            InputProps={{
                startAdornment: (
                    <InputAdornment position="start">
                        <IconButton
                            disabled={!status[offer.id] || status[offer.id].value <= calcMinRequest(offer.my_max_qty)}
                            color="secondary"
                            edge="start"
                            aria-label="Remove Qty"
                            onClick={handleQtyDec(offer)}>
                            <RemoveCircleOutlineIcon/>
                        </IconButton>
                    </InputAdornment>
                ),
                endAdornment: (
                    <InputAdornment position="end">
                        <IconButton
                            disabled={!status[offer.id] || status[offer.id].value >= MAX_REQUEST}
                            color="secondary"
                            edge="end"
                            aria-label="Add Qty"
                            onClick={handleQtyInc(offer)}>
                            <AddCircleOutlineIcon/>
                        </IconButton>
                    </InputAdornment>
                ),
            }}
        />
    );
};

const Customer = (props) => {
    const classes = useStyles();

    const dispatch = useDispatch();

    const offersSelector = state => state.offers.data;
    const filterSelector = state => state.filter;
    const offers = useSelector(createSelector([offersSelector, filterSelector],
        (offers, filter) => {
            return filterOffers(offers, filter);
        }));

    getWs((e) => {
        console.log('data received');

        dispatch(receiveOffers(JSON.parse(e.data)));
    });

    const [open, setOpen] = useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };


    return (
        <Paper className={classes.root}>
            <Box className={classes.buttonPanel}>
                <Button variant='contained' color='primary' onClick={handleClickOpen}>Inquiry</Button>
            </Box>
            <Table className={classes.table} size="small">
                <TableHead>
                    <TableRow>
                        <TableCell>Fav</TableCell>
                        <TableCell>Name</TableCell>
                        <TableCell>Price</TableCell>
                        <TableCell>Alt Price</TableCell>
                        <TableCell>Price Bet</TableCell>
                        <TableCell className={classes.requestCell}>QTY</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {offers.map((offer) => (
                        <TableRow key={offer.id}>
                            <TableCell padding="checkbox"><FavoriteCheckbox offer={offer}/></TableCell>
                            <TableCell>{offer.product_variant.name} {offer.description.name}</TableCell>
                            <TableCell>
                                {offer.is_inquiry ? 'Inquiry' : currencyFormatter().format(offer.price)}
                            </TableCell>
                            <TableCell>
                                {offer.is_inquiry ? 'Inquiry' : currencyFormatter(offer.alt_currency)
                                    .format(offer.alt_price)}
                            </TableCell>
                            <TableCell>{currencyFormatter().format(offer.price_delta)}</TableCell>
                            <TableCell><CustomerRequest offer={offer} className={classes.request}/></TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
            <CustomerInquiryDialog open={open} handleClose={handleClose}/>
        </Paper>
    )

};

export default Customer;
