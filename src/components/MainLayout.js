import React, {useState} from 'react';
import AppBar from '@material-ui/core/AppBar';
import Drawer from '@material-ui/core/Drawer';
import Avatar from '@material-ui/core/Avatar';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import SettingsIcon from '@material-ui/icons/Settings';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import {makeStyles, useTheme} from '@material-ui/core/styles';
import {useSelector} from "react-redux";

const drawerWidth = 300;

const useStyles = makeStyles(theme => ({
    drawerRoot: {
        marginLeft: 24,
    },
    root: {
        display: 'flex',
    },
    drawer: {
        [theme.breakpoints.up('sm')]: {
            width: drawerWidth,
            flexShrink: 0,
        },
    },

    appBar: {
        marginLeft: drawerWidth,
        backgroundColor: '#616161',
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
        },
    },
    menuButton: {
        marginRight: theme.spacing(2),
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
        width: drawerWidth,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
    title: {
        flexGrow: 1
    },
    avatar: {
        margin: 7,
        width: 40,
        height: 40,
    },
    grow: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    },
}));

const MainLayout = props => {
    const {container} = props;
    const classes = useStyles();
    const theme = useTheme();
    const [mobileOpen, setMobileOpen] = useState(false);
    const auth = useSelector(state => state.auth);
    const user_name = auth.user.name;

    function handleDrawerToggle() {
        setMobileOpen(!mobileOpen);
    }

    function logout() {
        document.cookie = "sso=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
        window.location.href='/back/logout'
    }

    const drawer = (
        <div style={{width:'90%'}}>
            <div className={classes.toolbar}/>
            {props.drawer}
           
        </div>
    );

    return (
        <div className={classes.root}>
            <AppBar position="fixed" className={classes.appBar}>
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="Open drawer"
                        edge="start"
                        onClick={handleDrawerToggle}
                        className={classes.menuButton}
                    >
                        <MenuIcon/>
                    </IconButton>
                    <Typography variant="h6" noWrap className={classes.title}>
                        Trade Zone
                    </Typography>
                    <div className={classes.grow}>

                    <div style={{marginRight:20}}><span style={{color:'#C1C1C1'}}>EUR/USD</span> <span style={{color:'#7BDCB5'}}>1.10</span> 
                    <span style={{color:'#C1C1C1', marginLeft:10}}>RUR/USD</span> <span style={{color:'#7BDCB5'}}>64.82</span></div>

                    <Avatar alt="Remy Sharp" src="https://material-ui.com/static/images/avatar/1.jpg" className={classes.avatar} />
                    {user_name}
                        <IconButton
                            edge='end'
                            aria-label='Profile'
                            onClick={() => window.location.href=process.env.REACT_APP_URL_PROFILE}
                            color='inherit'
                        >
                            <SettingsIcon/>
                        </IconButton>
                        <IconButton
                            edge='end'
                            aria-label='Logout'
                            onClick={() => logout()}
                            color='inherit'
                        >
                            <ExitToAppIcon/>
                        </IconButton>
                    </div>
                </Toolbar>
            </AppBar>
            <nav className={classes.drawer} aria-label="Mailbox folders">
                {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
                <Hidden smUp implementation="css">
                    <Drawer
                        container={container}
                        variant="temporary"
                        anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                        open={mobileOpen}
                        onClose={handleDrawerToggle}
                        classes={{
                            paper: classes.drawerPaper,
                        }}
                        ModalProps={{
                            keepMounted: true, // Better open performance on mobile.
                        }}
                    >
                        {drawer}
                    </Drawer>
                </Hidden>
                <Hidden xsDown implementation="css">
                    <Drawer
                        classes={{
                            paper: classes.drawerPaper,
                        }}
                        variant="permanent"
                        open
                    >
                        {drawer}
                    </Drawer>
                </Hidden>
            </nav>
            <main className={classes.content}>
                <div className={classes.toolbar}/>
                {props.children}
            </main>
        </div>
    );
};

export default MainLayout;