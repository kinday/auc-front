import React from 'react';
import { useTranslation } from 'react-i18next';
import Box from "@material-ui/core/Box";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ShopIcon from '@material-ui/icons/Shop';
import BarChartIcon from '@material-ui/icons/BarChart';
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import ImportantDevicesIcon from '@material-ui/icons/ImportantDevices'
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import SettingsApplicationsIcon from '@material-ui/icons/SettingsApplications';

// NOTE: Не могу сказать, что вижу в этом страшную проблему, но зачем
//       полагаться на перенаправление через JS, когда можно использовать
//       старые добрые ссылки?
//
//       <ListItem
//         button
//         onClick={() => window.location.href=CONSTANT}
//       >
//
//       ↓
//
//       <a href={CONSTANT}>
//         <ListItem button>...</ListItem>
//       </a>

const OrdersPanel = (props) => {
    const { t } = useTranslation();

    return (<Box>
        <Divider/>
        <List>
            <ListItem button onClick={() => window.location.href=process.env.REACT_APP_URL_PRODUCT_CATALOG}>
                <ListItemIcon>
                    <ImportantDevicesIcon/>
                </ListItemIcon>
                <ListItemText className='menu-item-text'
                              primary={t('menu.products.name')}/>
            </ListItem>

            <ListItem button onClick={() => window.location.href=process.env.REACT_APP_URL_PRICE_ANALYZER}>
                <ListItemIcon>
                    <BarChartIcon/>
                </ListItemIcon>
                <ListItemText className='menu-item-text'
                              primary={t('menu.priceAnalyzer.name')}/>
            </ListItem>
            <ListItem button onClick={() => window.location.href=process.env.REACT_APP_URL_OFFERS}>
                <ListItemIcon>
                    <AttachMoneyIcon/>
                </ListItemIcon>
                <ListItemText className='menu-item-text'
                              primary={t('menu.offers.name')}/>
            </ListItem>
            <ListItem button onClick={() => window.location.href=process.env.REACT_APP_URL_MANAGE}>
                    <ListItemIcon>
                        <SettingsApplicationsIcon/>
                    </ListItemIcon>
                    <ListItemText className='menu-item-text'
                                  primary={t('menu.manage.name')}/>
                </ListItem>
        </List>
        <Divider/>
    </Box>);
};

export default OrdersPanel;
