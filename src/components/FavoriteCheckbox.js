import React from 'react';

import {useDispatch} from "react-redux";
import Checkbox from "@material-ui/core/Checkbox";

import {changeFavorite} from "../actions";

const FavoriteCheckbox = props => {

    const offer = props.offer;
    const dispatch = useDispatch();

    const handleFavoriteChanged = (variantId) => event => {
        let favorite = event.target.checked;

        dispatch(changeFavorite(variantId, favorite));
    };

    return (<Checkbox checked={offer.additional.favorite} onChange={handleFavoriteChanged(offer.id)}/>)
};

export default FavoriteCheckbox;
