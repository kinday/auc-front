import React, {useState} from 'react';
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import {makeStyles} from "@material-ui/core";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormLabel from "@material-ui/core/FormLabel";
import RadioGroup from "@material-ui/core/RadioGroup";
import Radio from "@material-ui/core/Radio";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import {getDescriptions, getVariants, productInquiry} from "../services";
import {useSelector} from "react-redux";
import FormHelperText from "@material-ui/core/FormHelperText";
import TextField from "@material-ui/core/TextField";
import {useSnackbar} from 'notistack';
import {useTranslation} from "react-i18next";

const useStyles = makeStyles(theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection: 'column'
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 220
    }
}));

const CustomerInquiryDialog = (props) => {
    const classes = useStyles();
    const {enqueueSnackbar} = useSnackbar();
    const {t} = useTranslation();


    const {open, handleClose} = props;

    const brands = useSelector(state => state.brands.data);
    const categories = useSelector(state => state.categories.data);

    const [value, setValue] = useState({
        brand_id: 0,
        category_id: 0,
        variant_id: 0,
        description_id: 0,
        qty: 1
    });

    const dataSelected = () => {
        return !(value.brand_id && value.category_id && value.variant_id && value.description_id && value.qty > 0)
    };

    const [variants, setVariants] = useState([]);
    const [descriptions, setDescriptions] = useState([]);
    const [variantActive, setVariantActive] = useState(true);

    // NOTE: Возможно я что-то не знаю о самой задаче, но создание таких
    //       «универсальных» функций — скользкая дорожка. Со временем, даже
    //       если не появляется новая логика, а чаще всего она появляется,
    //       поддерживать их становится весьма трудно.
    //
    //       По хорошему, на разные типы данных должны быть разные обработчики.
    //       А ещё потенциально это всё может находиться в разных компонентах.
    //       Дублирование, если оно будет, можно либо решить выносом схожей
    //       логики во вспомогательные общие методы, либо просто пренебречь.
    const handleChange = name => event => {

        // NOTE: `useState` предназначен для примитивов. В принципе, объекты и
        //       массивы в нём можно хранить, если они изменяются целиком. Если
        //       же объект состоит из свойств, которые могут меняться
        //       независимо, рекомендуется либо использовать отдельный
        //       `useState` на каждое свойство или `useReducer`.
        const newValue = {...value, [name]: Number(event.target.value)};
        setValue(newValue);

        if ((event.target.name === 'category' || event.target.name === 'brand') && (newValue.brand_id && newValue.category_id)) {
            setVariantActive(false);
            getVariants(newValue.category_id, newValue.brand_id).then(response => {
                setValue({...newValue, 'variant_id': 0, 'description_id': 0});
                setVariants(response.data);
                setDescriptions([]);
                setVariantActive(true);
            });
        }

        if (event.target.name === 'variant' && newValue.variant_id) {

            const found = variants.find(e => e.id === newValue.variant_id);

            if (found) {
                getDescriptions(found.product_id).then(response => {
                    setValue({...newValue, 'description_id': 0});
                    setDescriptions(response.data)
                });
            }
        }
    };

    const handleInquiry = () => {

        productInquiry(value).then(() => {
            handleClose();
        }).catch(function () {
            enqueueSnackbar(t('error.productExists'), {variant: 'error'});
        });
    };


    return (<Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
    >
        <DialogTitle id="alert-dialog-title">Product inquiry</DialogTitle>
        <DialogContent>
            <form className={classes.container}>
                <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="category-select">Category</InputLabel>
                    <Select
                        inputProps={{
                            name: 'category',
                            id: 'category-select',
                        }}
                        value={value.category_id}
                        onChange={handleChange('category_id')}
                    >
                        {categories.map((category) => (
                            <MenuItem key={category.id} value={category.id}>{category.name}</MenuItem>
                        ))}
                    </Select>
                </FormControl>
                <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="brand-select">Brand</InputLabel>
                    <Select
                        inputProps={{
                            name: 'brand',
                            id: 'brand-select',
                        }}
                        value={value.brand_id}
                        onChange={handleChange('brand_id')}
                    >
                        {brands.map((brand) => (
                            <MenuItem key={brand.id} value={brand.id}>{brand.name}</MenuItem>
                        ))}
                    </Select>
                </FormControl>
                <FormControl className={classes.formControl} disabled={!variantActive}>
                    <InputLabel htmlFor="variant-select">Product Variant</InputLabel>
                    <Select
                        inputProps={{
                            name: 'variant',
                            id: 'variant-select'
                        }}
                        value={value.variant_id}
                        onChange={handleChange('variant_id')}
                    >
                        {variants.map((variant) => (
                            <MenuItem key={variant.id} value={variant.id}>{variant.name}</MenuItem>
                        ))}
                    </Select>
                    {!variantActive ? (<FormHelperText>Loading...</FormHelperText>) : ''}
                </FormControl>
                <FormControl component="fieldset" className={classes.formControl}>
                    <FormLabel component="legend">Descriptions</FormLabel>
                    <RadioGroup aria-label="description" name="description"
                                value={value.description_id}
                                onChange={handleChange('description_id')}
                    >
                        {descriptions.map((description) => (
                            <FormControlLabel key={description.id} value={description.id} control={<Radio/>}
                                              label={description.name}/>
                        ))}
                    </RadioGroup>
                </FormControl>
                <FormControl component="fieldset" className={classes.formControl}>
                    <TextField
                        id="standard-number"
                        label="Qty"
                        value={value.qty}
                        onChange={handleChange('qty')}
                        type="number"
                        InputLabelProps={{
                            shrink: true
                        }}
                        inputProps={{min: 1}}
                    />
                </FormControl>

            </form>
        </DialogContent>
        <DialogActions>
            <Button variant='contained' onClick={handleInquiry} color="primary" disabled={dataSelected()} autoFocus>
                Inquiry
            </Button>
            <Button onClick={handleClose} variant="outlined" color='secondary'>
                Cancel
            </Button>
        </DialogActions>
    </Dialog>);
};

export default CustomerInquiryDialog;
