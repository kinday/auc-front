import React, {useEffect, useRef, useState} from 'react';
import {getDocuments, getUserOrders} from "../services";
import {currencyFormatter} from "../config";
import CircularProgress from "@material-ui/core/CircularProgress";
import {makeStyles} from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import Grid from "@material-ui/core/Grid";
import ReactToPrint from "react-to-print";
import Button from "@material-ui/core/Button";
import {Link} from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import {useSelector} from "react-redux";

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing(3),
        padding: theme.spacing(3),
        overflowX: 'auto',
    },
    table: {
        marginTop: theme.spacing(2),
        minWidth: 650,
    },
    progress: {
        textAlign: 'center'
    },
    fab: {
        margin: theme.spacing(0)
    }
}));

const Document = ({match}) => {
    const classes = useStyles();
    const componentRef = useRef();

    const docId = match.params.docId;
    const [orders, setOrders] = useState({'items': []});
    const [loading, setLoading] = useState(false);
    const auth = useSelector(state => state.auth);

    useEffect(() => {
        getDocuments().then(response => {
            fetchOrders(docId);
            if (response.data.length > 0) {
            }
        });
    }, []);

    const fetchOrders = (date) => {
        setLoading(true);
        getUserOrders(date).then(response => {
            setOrders(response.data);
            setLoading(false);
        });
    };

    const orderName = () => auth.type === 'customer' ? 'Pushes order' : 'Sales order';

    return (<Paper className={classes.root}>
        <Grid container
              justify="space-between"
              alignItems="flex-end"
        >
            <Grid item>
                <Button component={Link} variant='contained'
                        to='/docs'
                        color='primary'>Back</Button>
            </Grid>
            <Grid item>
                <ReactToPrint
                    pageStyle=".MuiPaper-root {'color': 'black',
                        'background': 'white'}"
                    trigger={() => <Button variant="outlined" color='secondary'>Print</Button>}
                    content={() => componentRef.current}
                />
            </Grid>
        </Grid>
        {loading && (
            <div className={classes.progress}>
                <CircularProgress/>
            </div>
        )}
        <Grid ref={componentRef}
              container
              direction="column"
              justify="flex-start"
              alignItems="stretch"
        >
            <Grid item>
                <Typography align='center' variant="h5">
                    {orderName()}
                </Typography>
            </Grid>
            <Grid item>
                <Typography align='right'>
                    Date: {orders.date}
                </Typography>
            </Grid>
            <Grid item>
                <Table className={classes.table} size='small'>
                    <TableHead>
                        <TableRow>
                            <TableCell>#</TableCell>
                            <TableCell>Name</TableCell>
                            <TableCell>Price</TableCell>
                            <TableCell>Alt Price</TableCell>
                            <TableCell>Qty</TableCell>
                            <TableCell>Total</TableCell>
                            <TableCell>Alt Total</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {orders.items.map((item, index) => (
                            <TableRow key={item.id}>
                                <TableCell>{index + 1}</TableCell>
                                <TableCell>{item.product_variant.name} {item.description.name}</TableCell>
                                <TableCell>{currencyFormatter(item.currency).format(item.price)}</TableCell>
                                <TableCell>{currencyFormatter(item.alt_currency).format(item.alt_price)}</TableCell>
                                <TableCell>{item.qty}</TableCell>
                                <TableCell>{currencyFormatter(item.currency).format(item.price * item.qty)}</TableCell>
                                <TableCell>
                                    {currencyFormatter(item.alt_currency).format(item.alt_price * item.qty)}
                                </TableCell>
                            </TableRow>
                        ))}
                        <TableRow>
                            <TableCell/>
                            <TableCell/>
                            <TableCell/>
                            <TableCell/>
                            <TableCell>{orders.qty}</TableCell>
                            <TableCell>{currencyFormatter().format(orders.price)}</TableCell>
                            <TableCell>{currencyFormatter(orders.alt_currency).format(orders.alt_price)}</TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </Grid>
        </Grid>
    </Paper>);
};

export default Document;

