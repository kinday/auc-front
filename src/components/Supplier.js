import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {createSelector} from 'reselect';
import {makeStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import ThemeProvider from "@material-ui/styles/ThemeProvider";
import FavoriteCheckbox from "./FavoriteCheckbox";
import yellow from "@material-ui/core/colors/yellow";
import red from "@material-ui/core/colors/red";
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";

import {
    addOffer,
    initStatus,
    makeBet,
    lowerBet,
    priceChanged,
    receiveOffers,
    statusChanged,
    statusRemoved
} from '../actions';
import {filterOffers} from '../utils';
import {currencyFormatter} from "../config";
import {getWs} from "../services";

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing(3),
        overflowX: 'auto',
    },
    table: {
        minWidth: 850,
    },
    stock: {
        width: 50
    }
}));

const calculateStatus = (offer) => {
    if (offer.is_my_deal && offer.qty > offer.my_qty) {
        return offer.qty - offer.my_qty;
    }
    if (!offer.is_my_deal && offer.my_qty > 0) {
        return offer.my_qty;
    }

    return 0;
};

const addTheme = createMuiTheme({
    palette: {
        primary: yellow,
    },
});

const lostTheme = createMuiTheme({
    palette: {
        primary: red,
    },
});

const SupplierPrice = (props) => {
    const offer = props.offer;
    const alt = props.alt

    const status = useSelector(state => state.status);
    const dispatch = useDispatch();

    const handlePriceChanges = () => (event) => {
        dispatch(priceChanged(offer.id, event.target.value));
    };

    if (offer.price > 0) {
        if (offer.is_my_deal) {
            return currencyFormatter(alt ? offer.alt_currency : offer.currency).format(alt ? offer.alt_price : offer.price)
        } else {
            return currencyFormatter(alt ? offer.alt_currency : offer.currency).format(alt ? offer.alt_price : offer.next_price)
        }
    }

    return alt ? ('') : (<TextField type="number" value={status[offer.id] ? status[offer.id].price : 0}
                                    onChange={handlePriceChanges()}/>);
};

const SupplierDeal = (props) => {
    const offer = props.offer;
    const classes = props.classes;

    const status = useSelector(state => state.status);
    const dispatch = useDispatch();


    const handleSell = (offer) => event => {
        dispatch(statusRemoved(offer.id));
        lowerBet(offer.id, status[offer.id].value, offer.next_price);
    };

    const handleInquiry = (offer) => event => {
        dispatch(statusRemoved(offer.id));
        makeBet(offer.id, status[offer.id].value, status[offer.id].price);
    };

    const handleAdd = (offer) => event => {
        dispatch(statusRemoved(offer.id));
        addOffer(offer.id, status[offer.id].value);
    };

    if (offer.is_my_deal) {
        if (offer.qty <= offer.my_qty || !offer.is_winner) {
            return 'My Deal'
        } else {
            return (<ThemeProvider theme={addTheme}>
                <Button variant="outlined" color="primary"
                        disabled={!status[offer.id] || status[offer.id].value <= 0}
                        onClick={handleAdd(offer)}>Add</Button>
            </ThemeProvider>)
        }
    } else if (offer.is_inquiry) {
        return (<Button variant="outlined" color="secondary"
                        disabled={!status[offer.id] || status[offer.id].value <= 0 || status[offer.id].price <= 0}
                        onClick={handleInquiry(offer)}>Sell</Button>)
    } else if (offer.is_lost) {
        return (<ThemeProvider theme={lostTheme}>
            <Button variant="outlined" color="primary"
                    disabled={!status[offer.id] || status[offer.id].value <= 0}
                    onClick={handleSell(offer)}>Sell</Button>
        </ThemeProvider>)
    } else {
        return (
            <Button variant="outlined" color="secondary"
                    disabled={!status[offer.id] || status[offer.id].value <= 0}
                    onClick={handleSell(offer)}>Sell</Button>)
    }
};

const SupplierStatus = (props) => {

    const offer = props.offer;

    const status = useSelector(state => state.status);
    const dispatch = useDispatch();


    useEffect(() => {
        if (!status[offer.id] || !status[offer.id].manual) {
            dispatch(initStatus(offer.id, calculateStatus(offer)));
        }
    }, [offer]);

    const handleStatusChanges = () => (event) => {
        dispatch(statusChanged(offer.id, event.target.value));
    };

    if (!offer.is_my_deal || (offer.qty > offer.my_qty)) {
        return (<TextField type="number" value={status[offer.id] ? status[offer.id].value : ''}
                           onChange={handleStatusChanges()} className={props.className}/>);
    } else {
        return '';
    }
};


const Supplier = () => {
    const classes = useStyles();

    const dispatch = useDispatch();

    const offersSelector = state => state.offers.data;
    const filterSelector = state => state.filter;
    const offers = useSelector(createSelector([offersSelector, filterSelector],
        (offers, filter) => {
            return filterOffers(offers, filter);
        }));

    getWs((e) => {
        console.log('data received');

        dispatch(receiveOffers(JSON.parse(e.data)));
    });

    return (
        <Paper className={classes.root}>
            <Table className={classes.table}>
                <TableHead>
                    <TableRow>
                        <TableCell>Fav</TableCell>
                        <TableCell>Name</TableCell>
                        <TableCell>Price Bet</TableCell>
                        <TableCell>Alt Price Bet</TableCell>
                        <TableCell>QTY</TableCell>
                        <TableCell>Status</TableCell>
                        <TableCell>
                            Stock
                        </TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {offers.map((offer) => (
                        <TableRow key={offer.id}>
                            <TableCell padding="checkbox"><FavoriteCheckbox offer={offer}/></TableCell>
                            <TableCell>{offer.product_variant.name} {offer.description.name}</TableCell>
                            <TableCell><SupplierPrice offer={offer}/></TableCell>
                            <TableCell><SupplierPrice alt={true} offer={offer}/></TableCell>
                            <TableCell>{offer.qty + (offer.my_qty > 0 ? `/${offer.my_qty}` : '')}</TableCell>
                            <TableCell><SupplierDeal offer={offer} classes={classes}/></TableCell>
                            <TableCell><SupplierStatus offer={offer} className={classes.stock}/></TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </Paper>
    )
};

export default Supplier;