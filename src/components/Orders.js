import React, {useEffect, useRef, useState} from 'react';
import {makeStyles} from "@material-ui/core";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import Paper from "@material-ui/core/Paper";
import SearchIcon from "@material-ui/icons/Search";
import Fab from "@material-ui/core/Fab";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from "@material-ui/core/Button";
import {currencyFormatter, DATE_PATTERN, MIN_DOCUMENT_DATE} from "../config";
import OrderSales from "./OrderSales";
import OrderPushes from "./OrderPushes";
import ReactToPrint from "react-to-print";
import Box from "@material-ui/core/Box";
import {KeyboardDatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import {getDocumentDates, getOrders} from "../services";
import CircularProgress from "@material-ui/core/CircularProgress";

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing(3),
        overflowX: 'auto',
    },
    table: {
        minWidth: 650,
    },
    fab: {
        margin: theme.spacing(0)
    },
    progress: {
        textAlign: 'center'
    }
}));

const Orders = () => {
    const classes = useStyles();
    const dateUtils = new DateFnsUtils();
    const componentRef = useRef();

    const [selectedDate, setSelectedDate] = useState();
    const [disabledPicker, setDisabledPicker] = useState(true);
    const [dates, setDates] = useState([]);
    const [open, setOpen] = useState(false);
    const [currentOrder, setCurrentOrder] = useState({'user': {}});
    const [loading, setLoading] = useState(false);
    const [orders, setOrders] = useState([]);

    const fetchOrders = (date) => {
        let s = null;
        if (date) {
            s = new DateFnsUtils().format(date, DATE_PATTERN);
        }
        setLoading(true);
        getOrders(s).then(response => {
            const data = response.data;
            setOrders(data);
            setLoading(false);
        });
    };


    useEffect(() => {
        getDocumentDates().then(response => {
            setDates(response.data);
            if (response.data.length > 0) {
                const date = dateUtils.parse(response.data[0], DATE_PATTERN);
                setDisabledPicker(false);
                setSelectedDate(date);
                fetchOrders(date);
            }
        });
    }, []);

    const handleDateChange = (date) => {
        setSelectedDate(date);
        fetchOrders(date);
    };

    const handleClickOpen = (order) => (event) => {
        setCurrentOrder(order);
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const shouldDisableDate = (day) => {
        const s = dateUtils.format(day, DATE_PATTERN);

        return !dates.includes(s);
    };

    return (<Paper className={classes.root}>
        <Box m={2}>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <KeyboardDatePicker
                    minDate={new DateFnsUtils().addDays(new Date(), -MIN_DOCUMENT_DATE)}
                    disableFuture="true"
                    disabled = {disabledPicker}
                    shouldDisableDate={shouldDisableDate}
                    disableToolbar
                    variant="inline"
                    format="dd MMM yyyy"
                    margin="normal"
                    id="date-picker-inline"
                    label="Document date"
                    value={selectedDate}
                    onChange={handleDateChange}
                    KeyboardButtonProps={{
                        'aria-label': 'change date',
                    }}
                />
            </MuiPickersUtilsProvider>
        </Box>
        {loading && (
            <div className={classes.progress}>
                <CircularProgress/>
            </div>
        )}
        <Table className={classes.table}>
            <TableHead>
                <TableRow>
                    <TableCell>Date</TableCell>
                    <TableCell>Type</TableCell>
                    <TableCell>Number</TableCell>
                    <TableCell>Total</TableCell>
                    <TableCell>Qty</TableCell>
                    <TableCell/>
                </TableRow>
            </TableHead>
            <TableBody>
                {orders.map((order) => (
                    <TableRow key={order.user.id + order.type}>
                        <TableCell>{order.date}</TableCell>
                        <TableCell>{order.type}</TableCell>
                        <TableCell>{order.user.name}</TableCell>
                        <TableCell>{currencyFormatter().format(order.price)}</TableCell>
                        <TableCell>{order.qty}</TableCell>
                        <TableCell>
                            <Fab size="small" color="secondary" aria-label="View" className={classes.fab}
                                 onClick={handleClickOpen(order)}>
                                <SearchIcon/>
                            </Fab>
                        </TableCell>
                    </TableRow>
                ))}
            </TableBody>
        </Table>
        <Dialog
            fullWidth={true}
            maxWidth={'lg'}
            open={open}
            onClose={handleClose}
            aria-labelledby="max-width-dialog-title"
        >
            <DialogTitle id="max-width-dialog-title">{`${currentOrder.type} ${currentOrder.user.name}`}</DialogTitle>
            <DialogContent>
                {currentOrder.type ? (currentOrder.type === 'sales_order'
                    ? <OrderSales ref={componentRef} order={currentOrder}/>
                    : <OrderPushes ref={componentRef} order={currentOrder}/>) : ''}
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose} color="secondary">
                    Close
                </Button>
                <ReactToPrint
                    pageStyle=".MuiPaper-root {'color': 'black',
                        'background': 'white'}"
                    trigger={() => <Button>Print</Button>}
                    content={() => componentRef.current}
                />
            </DialogActions>
        </Dialog>
    </Paper>);
};

export default Orders;