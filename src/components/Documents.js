import React, {useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import {getDocuments} from "../services";
import {currencyFormatter} from "../config";
import CircularProgress from '@material-ui/core/CircularProgress';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from "@material-ui/icons/Search";
import {Link} from "react-router-dom";

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing(3),
        padding: theme.spacing(3),
        overflowX: 'auto',
    },
    table: {
        marginTop: theme.spacing(2),
        minWidth: 400,
    },
    progress: {
        textAlign: 'center'
    },
    fab: {
        margin: theme.spacing(0)
    }
}));

const Documents = () => {
    const classes = useStyles();

    const [documents, setDocuments] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        setLoading(true);
        getDocuments().then(response => {
            setDocuments(response.data);
            setLoading(false);
        });
    }, []);

    return (
        <Paper className={classes.root}>
            {loading && (
                <div className={classes.progress}>
                    <CircularProgress/>
                </div>
            )}
            <Table className={classes.table} size='small'>
                <TableHead>
                    <TableRow>
                        <TableCell>Date</TableCell>
                        <TableCell>Amount</TableCell>
                        <TableCell>Alt Amount</TableCell>
                        <TableCell>Qty</TableCell>
                        <TableCell/>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {documents.map((item) => (
                        <TableRow key={item._id}>
                            <TableCell>{item._id}</TableCell>
                            <TableCell>{currencyFormatter(item.currency).format(item.price)}</TableCell>
                            <TableCell>{currencyFormatter(item.alt_currency).format(item.alt_price)}</TableCell>
                            <TableCell>{item.qty}</TableCell>
                            <TableCell>
                                <IconButton color="secondary" aria-label="Open document"
                                            component={ Link } variant="contained"
                                            to={'/docs/' + item.date}
                                            className={classes.fab}>
                                    <SearchIcon/>
                                </IconButton>
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </Paper>

    );
};

export default Documents;
