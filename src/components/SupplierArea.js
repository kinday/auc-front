import React from 'react';
import Supplier from "./Supplier";
import {useSelector} from "react-redux";
import AuctionStub from "./ActionStub";

const SupplierArea = () => {

    const auth = useSelector(state => state.auth);

    const isAuctionActive = () => {
        const now = new Date();

        return now > new Date(auth.user.startDate) && now < new Date(auth.user.finishDate);
    };

    return isAuctionActive() ? (<Supplier/>) : (<AuctionStub/>);
};

export default SupplierArea;

