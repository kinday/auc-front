import React, {useEffect} from 'react';
import {fetchUserDetails} from "../actions";
import {useDispatch, useSelector} from "react-redux";
import {BrowserRouter as Router, Route} from "react-router-dom";
import LoadingPage from "./LoadingPage";
import MainLayout from "./MainLayout";
import FilterPanel from "./FilterPanel";
import Orders from "./Orders";
import OrdersPanel from "./OrdersPanel";
import Documents from "./Documents";
import Document from "./Document";
import CustomerArea from "./CustomerArea";
import SupplierArea from "./SupplierArea";

const Main = (props) => {

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchUserDetails())
    }, []);

    const auth = useSelector(state => state.auth);

    let content;
    let drawer = <FilterPanel/>;

    switch (auth.type) {
        case 'customer':
            content = (
                <>
                    <Route exact path='/' component={CustomerArea}/>
                    <Route exact path='/docs' component={Documents}/>
                    <Route path={'/docs/:docId'} component={Document} />
                </>
            );
            break;
        case 'admin':
            content = <Orders/>;
            drawer = <OrdersPanel/>;
            break;
        default:
            content = (
                <>
                    <Route exact path="/" component={SupplierArea}/>
                    <Route exact path="/docs" component={Documents}/>
                    <Route path={'/docs/:docId'} component={Document} />
                </>
            );
    }

    if (auth.user && auth.user.profile && auth.user.profile.theme) {
        props.toggleTheme(auth.user.profile.theme);
    }

    return auth.loggedIn ? (
        <Router>
            <MainLayout drawer={drawer}>
                {content}
            </MainLayout>
        </Router>
    ) : (
        <LoadingPage/>
    )
};

export default Main;