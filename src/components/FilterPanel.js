import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import { useTranslation } from 'react-i18next';
import {Link} from "react-router-dom";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Box from "@material-ui/core/Box";
import TextField from "@material-ui/core/TextField";
import {fetchBrands, fetchCategories} from "../actions";
import {actionConsts} from "../config";
import {makeStyles} from "@material-ui/core";
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import ShopIcon from '@material-ui/icons/Shop';
import BarChartIcon from '@material-ui/icons/BarChart';
import DescriptionIcon from '@material-ui/icons/Description'
import ImportantDevicesIcon from '@material-ui/icons/ImportantDevices'
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import SettingsApplicationsIcon from '@material-ui/icons/SettingsApplications';

const useStyles = makeStyles(theme => ({
    fieldset: {
        width: '100%',
        marginLeft: theme.spacing(2)
    },
    legend: {
        ...theme.typography.h5,
        paddingTop: theme.spacing(2)
    }
}));

const FilterPanel = () => {

    const classes = useStyles();
    const { t } = useTranslation();

    const form = useSelector(state => state.filter);
    const brands = useSelector( state => state.brands.data);
    const categories = useSelector( state => state.categories.data);
    const auth = useSelector(state => state.auth);
    
    const dispatch = useDispatch();

    const handleFilterChanged = (name, type) => event => {
        let f = {...form};
        f[type] = {...f[type]};
        f[type][name] = event.target.checked;

        dispatch({type: actionConsts.TOGGLE_FILTER, form: f});
    };

    const handleFavoriteChanged = () => event => {
        dispatch({type: actionConsts.TOGGLE_FILTER, form: {...form, favorite: event.target.checked}});
    };

    const handleInStockChanged = () => event => {
        dispatch({type: actionConsts.TOGGLE_FILTER, form: {...form, inStock: event.target.checked}});
    };

    const handleSearchChanged = () => event => {
        dispatch({type: actionConsts.TOGGLE_FILTER, form: {...form, search: event.target.value}});
    };

    useEffect(() => {
        dispatch(fetchBrands());
        dispatch(fetchCategories());
    }, []);

    return (
        <Box>
            <Divider/>
            <List>
                {(auth.type !== 'supplier' && auth.type !== 'customer') &&
                (
                <ListItem button onClick={() => window.location.href=process.env.REACT_APP_URL_PRODUCT_CATALOG}>
                    <ListItemIcon>
                        <ImportantDevicesIcon/>
                    </ListItemIcon>
                    <ListItemText className='menu-item-text'
                                  primary={t('menu.products.name')}/>
                </ListItem>
                )}
                {(auth.type === 'supplier' && auth.type !== 'customer') && (   
                <ListItem button onClick={() => window.location.href=process.env.REACT_APP_URL_PRICE_ANALYZER}>
                    <ListItemIcon>
                        <BarChartIcon/>
                    </ListItemIcon>
                    <ListItemText className='menu-item-text'
                                  primary={t('menu.priceAnalyzer.name')}/>
                </ListItem>
                )}
                 {(auth.type !== 'supplier' && auth.type !== 'customer') &&
                (<>
                <ListItem button onClick={() => window.location.href=process.env.REACT_APP_URL_OFFERS}>
                    <ListItemIcon>
                        <ShopIcon/>
                    </ListItemIcon>
                    <ListItemText className='menu-item-text'
                                  primary={t('menu.offers.name')}/>
                </ListItem>
                <ListItem button onClick={() => window.location.href=process.env.REACT_APP_URL_MANAGE}>
                    <ListItemIcon>
                        <SettingsApplicationsIcon/>
                    </ListItemIcon>
                    <ListItemText className='menu-item-text'
                                  primary={t('menu.manage.name')}/>
                </ListItem>
                    </>
                )}
                <ListItem button to="/docs" component={Link}>
                    <ListItemIcon>
                        <DescriptionIcon/>
                    </ListItemIcon>
                    <ListItemText className='menu-item-text'
                                  primary='Documents'/>
                </ListItem>
                <ListItem button to="/" component={Link}>
                    <ListItemIcon>
                        <AttachMoneyIcon/>
                    </ListItemIcon>
                    <ListItemText className='menu-item-text'
                                  primary={t('menu.tradeZone.name')}/>
                </ListItem>
            </List>
            <Divider/>
            <form noValidate autoComplete="off">
                <TextField  className={classes.fieldset}
                    id="standard-name"
                    label="Filter by name"
                    value={form.search}
                    onChange={handleSearchChanged()}
                    margin="normal"
                    type="search"

                />
            </form>
            <FormControl component="fieldset" className={classes.fieldset}>
                <FormGroup>
                    <FormControlLabel
                        control={<Checkbox onChange={handleFavoriteChanged()}
                                           checked={form.favorite}
                        />}
                        label="Favorite"
                    />
                </FormGroup>
            </FormControl>
            <FormControl component="fieldset" className={classes.fieldset} >
                <FormGroup>
                    <FormControlLabel
                        control={<Checkbox onChange={handleInStockChanged()}
                                           checked={form.inStock}
                        />}
                        label={auth.type === 'supplier' ? 'In stock' : 'In process'}
                    />
                </FormGroup>
            </FormControl>
            <FormControl component="fieldset"  className={classes.fieldset}>
                <FormLabel component="legend" className={classes.legend}>Brand</FormLabel>
                <FormGroup>
                    {brands.map((brand, index) => (
                        <FormControlLabel key={index}
                        control={<Checkbox value={brand.id} onChange={handleFilterChanged(brand.id, 'brand')}
                                           checked={form.brand[brand.id]}
                        />}
                        label={brand.name}/>)
                    )}
                </FormGroup>
            </FormControl>
            <FormControl component="fieldset" className={classes.fieldset}>
                <FormLabel component="legend" className={classes.legend}>Category</FormLabel>
                <FormGroup>
                    {categories.map((category, index) => (
                        <FormControlLabel key={index}
                            control={<Checkbox value={category.id} onChange={handleFilterChanged(category.id, 'category')}
                                               checked={form.category[category.id]}
                            />}
                            label={category.name}/>)
                    )}
                </FormGroup>
            </FormControl>
        </Box>
    );
};

export default FilterPanel;
