import {createRequest, getBrands, getCategories, getUserDetails, setFavorite} from "../services";
import {actionConsts} from "../config";

export {addOffer, makeBet, lowerBet} from '../services';

export const receiveOffers = (data) => {
    return {
        type: actionConsts.GET_OFFERS_SUCCESS,
        data: data
    }
};

const favoriteChanged = (variantId, data) => {
    return {
        type: actionConsts.FAVORITE_CHANGED,
        product_variant_id: variantId,
        data: data
    }
};

const userDetailsReceived = (data) => {
    return {
        type: actionConsts.USER_DETAILS_RECEIVER,
        user: data
    }
};

const receivedBrands = (data) => {
    return {
        type: actionConsts.GET_BRANDS_SUCCESS,
        data: data
    }
};

const receivedCategories = (data) => {
    return {
        type: actionConsts.GET_CATEGORIES_SUCCESS,
        data: data
    }
};

export const statusChanged = (offer, value) => {
    return {
        type: actionConsts.STATUS_CHANGED,
        offer: offer,
        value: value
    }
};

export const priceChanged = (offer, value) => {
    return {
        type: actionConsts.STATUS_PRICE_CHANGED,
        offer: offer,
        value: value
    }
};


export const statusRemoved = (offer) => {
    return {
        type: actionConsts.STATUS_REMOVED,
        offer: offer
    }
};

export const initStatus = (offer, value) => {
    return {
        type: actionConsts.STATUS_INIT,
        offer: offer,
        value: value
    }
};


export function changeFavorite(variantId, favorite) {
    return dispatch => {
        return setFavorite(variantId, favorite).then(response => {
            const data = response.data;

            dispatch(favoriteChanged(variantId, data))

        });
    }

}

export const fetchUserDetails = () => {
    return dispatch => {
        return getUserDetails().then(response => {
            const data = response.data;
            dispatch(userDetailsReceived(data));
        });
    }
};

export const fetchBrands = () => {
    return dispatch => {
        return getBrands().then(response => {
            const data = response.data;
            dispatch(receivedBrands(data));
        });
    }
};

export const fetchCategories = () => {
    return dispatch => {
        return getCategories().then(response => {
            const data = response.data;
            dispatch(receivedCategories(data));
        });
    }
};

export const makeRequest = (variantId, qty) => {
    return dispatch => {
        return createRequest(variantId, qty)
            .then(resp => {
                dispatch(initStatus(variantId, resp.data.qty));
            });
    }
};

